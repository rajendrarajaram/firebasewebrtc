package io.github.rajendrarajaram.firebasewebrtc;
import org.webrtc.MediaStream;

public class Callback {
    public interface GeneralCallback
    {
        void onException(Exception e);
        void onSocketEvent(String socketEvent);
    }

    public interface VideoChatCallback
    {
        void onInvite();
        void onAddStream(MediaStream mediaStream, String id, int endPoint);
        void onPoorConnection();
        void onRemoveStream(String socketId);

        void showProgress();

        void hideProgressBar();

        void onSmileIndex(float smileIndex);
    }


}
