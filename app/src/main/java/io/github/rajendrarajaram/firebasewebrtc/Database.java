package io.github.rajendrarajaram.firebasewebrtc;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SessionDescription;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.github.rajendrarajaram.firebasewebrtc.model.Data;
import io.github.rajendrarajaram.firebasewebrtc.model.SmileData;
import io.github.rajendrarajaram.firebasewebrtc.model.SubData;

public class Database implements ChildEventListener {
    private String TAG = "alakh";
    private FirebaseDatabase database;
    private DatabaseReference myRef_main;
    private DatabaseReference myRef_webRTC;
    private DatabaseReference myRef_Room;
    private DatabaseReference myRef_Action;
    private DatabaseReference myRef_invite;
    private DatabaseReference myRef_simleIndex;

    private MediaConstraints mediaConstraints;
    private MediaStream mediaStream_local;
    private Callback.GeneralCallback generalCallback;
    private Callback.VideoChatCallback videoChatCallback;
    private Peer peer;
    private PeerConnectionFactory factory;
    private String roomName;


    public Callback.GeneralCallback getGeneralCallback() {
        return generalCallback;
    }

    public void setGeneralCallback(Callback.GeneralCallback generalCallback) {
        this.generalCallback = generalCallback;
    }

    public Callback.VideoChatCallback getVideoChatCallback() {
        return videoChatCallback;
    }

    public void setVideoChatCallback(Callback.VideoChatCallback videoChatCallback) {
        this.videoChatCallback = videoChatCallback;
    }

    public MediaStream getMediaStream_local() {
        return mediaStream_local;
    }

    public void setMediaStream_local(MediaStream mediaStream_local) {
        this.mediaStream_local = mediaStream_local;
    }

    public MediaConstraints getMediaConstraints() {
        return mediaConstraints;
    }


    public Database() {
        database = FirebaseDatabase.getInstance();
        myRef_main = database.getReference();

        myRef_main.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                List<String> userids = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    userids.add(snapshot.getValue().toString());
                    Log.i(TAG, "Child " + snapshot.getValue().toString());
                }
                switch (userids.size()) {

                    case 0:
                        break;
                    case  1:
                        Log.i(TAG, "onChildAdded: ");
                        break;
                    case 2:
                        String key = myRef_invite.push().getKey();
                        myRef_invite.child(key).setValue(userids.get(0));
                        myRef_main.child("invite").removeValue();
                        break;
                    default:
                        myRef_main.child("room").removeValue();


                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        myRef_webRTC = myRef_main.child("webRtc");
        myRef_Action = myRef_main.child("action");
        myRef_invite = myRef_main.child("invite");
        myRef_simleIndex= myRef_main.child("smile");
        myRef_simleIndex.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                String smileIndex = (String ) dataSnapshot.getValue();
                Log.i(TAG, "remote "+dataSnapshot.getValue());

                SmileData smileData =  dataSnapshot.getValue(SmileData.class);
                if (!smileData.id.equals(peer.getPeerBuilder().id))


                videoChatCallback.onSmileIndex(smileData.smileIndex);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        myRef_Action.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String action = (String) dataSnapshot.getValue();
                Log.i(TAG, "action " + action);
                if (action.equals("disconnect")) {
                    videoChatCallback.onRemoveStream(peer.getPeerBuilder().id);
                    peer.release();
                    myRef_main.child(roomName).removeValue();


                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        myRef_invite.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String id = (String) dataSnapshot.getValue();
                if (peer == null) {
                    Log.e(TAG, "invite is not possbile peer does not exist ");
                    return;
                }

                if (id.equals(peer.getPeerBuilder().id)) {
                    videoChatCallback.showProgress();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            createOffer();
                            videoChatCallback.hideProgressBar();
                            myRef_main.child("invite").removeValue();
                        }
                    }, 5000);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        myRef_webRTC.addChildEventListener(this);
    }

    public void createRoom(String roomName) {
        this.roomName = roomName;

        myRef_Room = myRef_main.child(roomName);
        myRef_Room.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    void sendMessage(String senderId, SubData message) {

        Data data = new Data();
        data.setId(senderId);
        data.setSubData(message);

        final String key = myRef_webRTC.push().getKey();
        myRef_webRTC.child(key).setValue(data);
        myRef_main.child("webRtc").removeValue();
    }


    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Data data = dataSnapshot.getValue(Data.class);
        Log.i(TAG, "Child Count " + dataSnapshot.getChildrenCount());
        if (data == null) {
            Log.e(TAG, "response is null: ");
            return;
        }


        if (data.getId() != peer.getPeerBuilder().id) {
            if (data.getSubData() == null) {
                return;
            }



            if (data.getSubData().getIce() != null) {
                SubData.Ice ice = data.getSubData().getIce();
                IceCandidate candidate = new IceCandidate(
                        ice.getSdpMid(),
                        ice.getSdpMLineIndex(),
                        ice.getCandidate());

                peer.mPeerConnection.addIceCandidate(candidate);
            } else if (data.getSubData().getSdp() != null) {
                SubData.Sdp sdp = data.getSubData().getSdp();
                SessionDescription description = new SessionDescription(SessionDescription.Type.fromCanonicalForm(sdp.getType()), sdp.getSdp());

                if (sdp.getType().equals("offer")) {

                    peer.mPeerConnection.setRemoteDescription(peer, description);
                    peer.mPeerConnection.createAnswer(peer, mediaConstraints);

                } else {

                    peer.mPeerConnection.setRemoteDescription(peer, description);
                }

            }

        }

    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }


    public LinkedList<PeerConnection.IceServer> getIceServer() {
        LinkedList<PeerConnection.IceServer> iceServers = new LinkedList<>();
        iceServers.add(new PeerConnection.IceServer("stun:stun.services.mozilla.com"));
        iceServers.add(new PeerConnection.IceServer("stun:stun.l.google.com:19302"));
        iceServers.add(new PeerConnection.IceServer("stun:stun.services.mozilla.com", "beaver", "webrtc.websitebeaver@gmail.com"));
        return iceServers;
    }

    public void init(MediaStream local_mediaStream, PeerConnectionFactory factory, MediaConstraints mediaConstraint) {
        this.mediaStream_local = local_mediaStream;
        this.factory = factory;
        this.mediaConstraints = mediaConstraint;

        Peer.PeerBuilder peerBuilder = new Peer.PeerBuilder();
        peerBuilder.id = randomAlphaNumeric(5);
        peerBuilder.connectionDescription = null;
        peerBuilder.endPoint = 0;
        peerBuilder.mFactory = factory;
        room(peerBuilder.id);

        peer = new Peer(peerBuilder, this);

    }

    private void room(String id) {
        final String key = myRef_Room.push().getKey();
        myRef_Room.child(key).setValue(id);
    }

    public void createOffer() {
        peer.mPeerConnection.createOffer(peer, mediaConstraints);
    }

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }


    public void disconnect() {
        callAction("disconnect");
    }

    private void callAction(String disconnect) {
        final String key = myRef_Action.push().getKey();
        myRef_Action.child(key).setValue(disconnect);
        myRef_main.child("action").removeValue();
    }

    public  void sendSmile(float smileIndex)
    {

        String key = myRef_simleIndex.push().getKey();
        SmileData smileData = new SmileData();
        smileData.setId(peer.getPeerBuilder().id);
        smileData.setSmileIndex(smileIndex);
        myRef_simleIndex.child(key).setValue(smileData);
        myRef_main.child("smile").removeValue();

    }
}
