package io.github.rajendrarajaram.firebasewebrtc;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;

import java.util.List;

public class FbMachineLearning {
    private String TAG = "faceReading";

    private final FirebaseVisionFaceDetectorOptions options;
    private final FirebaseVisionFaceDetector detector;
    private FirebaseVisionImage image;
    private  FaceListener faceListener;
    public interface  FaceListener
    {
        public  void simpleIndex(float index);
    }

    public FbMachineLearning() {
         options =
                new FirebaseVisionFaceDetectorOptions.Builder()
                        .setModeType(FirebaseVisionFaceDetectorOptions.ACCURATE_MODE)
                        .setLandmarkType(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
                        .setClassificationType(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                        .setMinFaceSize(0.15f)
                        .setTrackingEnabled(true)
                        .build();

         detector = FirebaseVision.getInstance()
                .getVisionFaceDetector(options);
    }

        public void setBitmap(Bitmap bitmap) {

         image = FirebaseVisionImage.fromBitmap(bitmap);



    }

    public  void build(FaceListener faceListener)
    {
        this.faceListener  = faceListener;
        Task<List<FirebaseVisionFace>> result =
                detector.detectInImage(image)
                        .addOnSuccessListener(
                                new OnSuccessListener<List<FirebaseVisionFace>>() {
                                    @Override
                                    public void onSuccess(List<FirebaseVisionFace> faces) {
                                        if (faces.isEmpty())
                                        {
                                            return;
                                        }
                                        FirebaseVisionFace  firebaseVisionFace  = faces.get(0);
                                        faceAnalysis(firebaseVisionFace);
                                    }
                                })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                    }
                                });


    }

    private void faceAnalysis(FirebaseVisionFace face) {
        Rect bounds = face.getBoundingBox();
        float rotY = face.getHeadEulerAngleY();  // Head is rotated to the right rotY degrees
        float rotZ = face.getHeadEulerAngleZ();  // Head is tilted sideways rotZ degrees

        // If landmark detection was enabled (mouth, ears, eyes, cheeks, and
        // nose available):
        FirebaseVisionFaceLandmark leftEar = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EAR);
        if (leftEar != null) {
            FirebaseVisionPoint leftEarPos = leftEar.getPosition();

        }

        // If classification was enabled:
        if (face.getSmilingProbability() != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
            float smileProb = face.getSmilingProbability();
            Log.i(TAG, "Smile index "+smileProb);
            faceListener.simpleIndex(smileProb);
        }
        if (face.getRightEyeOpenProbability() != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
            float rightEyeOpenProb = face.getRightEyeOpenProbability();
            Log.i(TAG, "Right Eye open "+rightEyeOpenProb);
        }

        // If face tracking was enabled:
        if (face.getTrackingId() != FirebaseVisionFace.INVALID_ID) {
            int id = face.getTrackingId();
            Log.i(TAG, "face Id"+id);
        }
    }


}
