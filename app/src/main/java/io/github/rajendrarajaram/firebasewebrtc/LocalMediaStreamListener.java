package io.github.rajendrarajaram.firebasewebrtc;

import android.graphics.Bitmap;

import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnectionFactory;

public interface LocalMediaStreamListener {

    public void onLocalStream(MediaStream local_mediaStream, PeerConnectionFactory factory, MediaConstraints mediaConstraint);

}
