package io.github.rajendrarajaram.firebasewebrtc;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import org.webrtc.EglBase;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;

import io.github.rajendrarajaram.firebasewebrtc.ui.BaseActivity;
import io.github.rajendrarajaram.firebasewebrtc.ui.VideoConsole;

public class MainActivity extends BaseActivity implements LocalMediaStreamListener, View.OnClickListener, Callback.GeneralCallback, Callback.VideoChatCallback, CustomViewRenderer.BitmapCallback {
    public VideoView mVideoView_local;
    private RendererCommon.ScalingType scalingType = RendererCommon.ScalingType.SCALE_ASPECT_FILL;
    private EglBase rootEglBase;
    private FloatingActionButton floatingActionButton;
    private Database database;
    private VideoView videoView_remote;
    private VideoConsole videoConsole;
    private boolean isRemote;
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, 0);
    private String TAG ="alakh";
    private  FbMachineLearning learning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        ((PowerManager)getSystemService(POWER_SERVICE)).newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "TAG").acquire();
learning  = new FbMachineLearning();
        mVideoView_local = findViewById(R.id.localView);
        videoView_remote = findViewById(R.id.remoteView);
        database = new Database();
        database.setGeneralCallback(this);
        database.setVideoChatCallback(this);
        floatingActionButton = findViewById(R.id.start_call);
        floatingActionButton.setOnClickListener(this);
        rootEglBase = EglBase.create();
        mVideoView_local.init(rootEglBase);
        mVideoView_local.setScalingType(scalingType);
        videoView_remote.init(rootEglBase);
        videoView_remote.setScalingType(scalingType);
        PeerConnectionParameters params = new PeerConnectionParameters(true, true, 50, 50, 25, 100, "VP9", true, 50, "opus", true);
        mVideoView_local.createLocalStream(params, this);
        mVideoView_local.rendererCallback(this);
        videoConsole = new VideoConsole(this);

    }



    @Override
    public void onLocalStream(final MediaStream local_mediaStream, final PeerConnectionFactory factory, final MediaConstraints mediaConstraint) {
        database.createRoom(getIntent().getStringExtra(RoomActivity.ROOMNAME));

        database.init(local_mediaStream, factory, mediaConstraint);
    }

    @Override
    public void onClick(View view) {
            database.disconnect();



    }

    @Override
    public void onException(Exception e) {

    }

    @Override
    public void onSocketEvent(String socketEvent) {

    }

    @Override
    public void onInvite() {

    }

    @Override
    public void onAddStream(final MediaStream mediaStream, String id, int endPoint) {
        Log.e("rajendra", "onAddStream: ");
        isRemote = true;
        runOnUiThread(new Runnable() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void run() {
                params.weight = .50f;
                mVideoView_local.setLayoutParams(params);
                videoView_remote.setRemoteMediaStream(mediaStream);
                floatingActionButton.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onPoorConnection() {

    }

    @Override
    public void onRemoveStream(String socketId) {
        params.weight = 1f;
        isRemote = true;
        mVideoView_local.setLayoutParams(params);
        mVideoView_local.removeMediaStream();

        finish();


    }

    @Override
    public void showProgress() {
        findViewById(R.id.progress).setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgressBar() {
        findViewById(R.id.progress).setVisibility(View.GONE);
    }

    @Override
    public void onSmileIndex(float smileIndex) {

  videoView_remote.simple(smileIndex);

    }

    @Override
    public void onRenderer(Bitmap bitmap) {
        learning.setBitmap(bitmap);
        learning.build(new FbMachineLearning.FaceListener() {
            @Override
            public void simpleIndex(float index) {
                mVideoView_local.simple(index);

                if (isRemote)
                    database.sendSmile(index);

            }
        });
    }

}
