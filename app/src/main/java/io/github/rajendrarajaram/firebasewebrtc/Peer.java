package io.github.rajendrarajaram.firebasewebrtc;


import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RtpReceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;

import io.github.rajendrarajaram.firebasewebrtc.model.Data;
import io.github.rajendrarajaram.firebasewebrtc.model.SubData;

public class Peer implements SdpObserver, PeerConnection.Observer {
    private static final String TAG = "codiant";
    private PeerBuilder mPeerBuilder;
    public PeerConnection mPeerConnection;
    private Database database;
    private MediaStream mediaStreamRemote;



    public PeerBuilder getPeerBuilder() {
        return mPeerBuilder;
    }

    public Peer(PeerBuilder peerBuilder, Database database) {
        this.database = database;
        mPeerBuilder = peerBuilder;
        mPeerConnection = mPeerBuilder.mFactory.createPeerConnection(database.getIceServer(), database.getMediaConstraints(), this);
        mPeerConnection.addStream(database.getMediaStream_local());
    }

    @Override
    public void onSignalingChange(PeerConnection.SignalingState signalingState) {

    }

    @Override
    public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
        switch (iceConnectionState) {
            case FAILED:
                database.getVideoChatCallback().onPoorConnection();
                break;
        }
    }

    @Override
    public void onIceConnectionReceivingChange(boolean b) {

    }

    @Override
    public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {

    }

    @Override
    public void onIceCandidate(IceCandidate candidate) {
  SubData  subData  = new SubData();

            SubData.Ice ice  = new SubData.Ice();
            ice.setCandidate(candidate.sdp);
            ice.setSdpMid(candidate.sdpMid);
            ice.setSdpMLineIndex(candidate.sdpMLineIndex);
            subData.setIce(ice);
            database.sendMessage(mPeerBuilder.id, subData);



    }

    @Override
    public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {

    }

    @Override
    public void onAddStream(MediaStream mediaStream) {
mediaStreamRemote  = mediaStream;
        database.getVideoChatCallback().onAddStream(mediaStream, mPeerBuilder.id, mPeerBuilder.endPoint + 1);
    }

    @Override
    public void onRemoveStream(MediaStream mediaStream) {

    }

    @Override
    public void onDataChannel(DataChannel dataChannel) {

    }

    @Override
    public void onRenegotiationNeeded() {

    }

    @Override
    public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {

    }

    @Override
    public void onCreateSuccess(SessionDescription sessionDescriptiondp) {
        SubData subData  = new SubData();

             SubData.Sdp sdp = new SubData.Sdp();
             sdp.setSdp(sessionDescriptiondp.description);

             if (sessionDescriptiondp.type == SessionDescription.Type.ANSWER) {
                 sdp.setType("answer");
                if (mPeerConnection.getLocalDescription() == null) {
                    Log.i(TAG, " EMMIT ANSWER");
                    subData.setSdp(sdp);
                    database.sendMessage(mPeerBuilder.id, subData);
//                    mWebSocket.mSocket.emit( WebRtcCustomConstant.SENDANSWER, myofferdescrition );
                }
            } else {
                 sdp.setType("offer");
                if (mPeerConnection.getLocalDescription() == null) {
                    Log.i(TAG, " EMMIT OFFER ");
                    subData.setSdp(sdp);
                    database.sendMessage(mPeerBuilder.id, subData);
                }
            }
            mPeerConnection.setLocalDescription(Peer.this, sessionDescriptiondp);
            }

    @Override
    public void onSetSuccess() {

    }

    @Override
    public void onCreateFailure(String s) {

    }

    @Override
    public void onSetFailure(String s) {

    }

    public void release() {

        if (mediaStreamRemote==null)
        {
            return;
        }

        mPeerConnection.removeStream(mediaStreamRemote);
        mPeerConnection.dispose();
        mPeerConnection = null;
    }

    public static class PeerBuilder {
        public PeerConnectionFactory mFactory;
        public String id;
        public int endPoint;
        public JSONObject connectionDescription;
}
}
