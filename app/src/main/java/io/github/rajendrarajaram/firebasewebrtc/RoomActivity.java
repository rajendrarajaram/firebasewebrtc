package io.github.rajendrarajaram.firebasewebrtc;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import io.github.rajendrarajaram.firebasewebrtc.ui.BaseActivity;

public class RoomActivity extends BaseActivity implements View.OnClickListener {
    public  static  final  String ROOMNAME="room";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ViewCompat.setElevation(((ImageView)findViewById(R.id.splash_logo)), 10);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.room_activity_fab);
        fab.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        String roomname  = ((EditText)findViewById(R.id.room_et)).getText().toString();
        if (roomname.isEmpty())
        {
            showSnakeBar(view,"enter room nanme");
            return;
        }
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra(ROOMNAME,roomname);

        startActivity(intent);

    }
}
