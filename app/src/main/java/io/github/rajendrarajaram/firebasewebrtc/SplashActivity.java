package io.github.rajendrarajaram.firebasewebrtc;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import io.github.rajendrarajaram.firebasewebrtc.ui.BaseActivity;

public class SplashActivity extends BaseActivity implements View.OnClickListener {
    private FloatingActionButton floatingActionButton_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
        ViewCompat.setElevation(((ImageView)findViewById(R.id.splash_logo)), 10);
        floatingActionButton_next = findViewById(R.id.splash_next_fab);
        floatingActionButton_next.setOnClickListener(this);
        checkRequiredPermission(CONSTANTS.PERMISSION);
    }

    @Override
    protected void invokedWhenPermissionGranted() {
        super.invokedWhenPermissionGranted();
    }

    private void delay() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callHome();
            }
        }, 5000);

    }

    private void callHome() {
        Intent intent = new Intent(this, RoomActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void invokedWhenNoOrAlreadyPermissionGranted() {
//delay();
    }

    @Override
    public void onClick(View view) {
        callHome();
    }
}
