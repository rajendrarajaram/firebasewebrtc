package io.github.rajendrarajaram.firebasewebrtc;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.hsalf.smilerating.SmileRating;

import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.EglBase;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoFrame;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.util.ArrayList;
import java.util.List;

public class VideoView extends WebRtcView {
    private static final String CONSTANT_VIDEO_ID = "video_id";
    private String TAG = VideoView.class.getSimpleName();
    private EglBase eglBase;
    private RendererCommon.ScalingType type;
    private MediaStream remoteStream;
    private VideoRenderer videoRenderer;
    private VideoTrack currentVideoTrack;
    private final List<VideoRenderer.Callbacks> videoViewCallback = new ArrayList<VideoRenderer.Callbacks>();
    private VideoCapturer videoCapturer;
    private PeerConnectionParameters peerConnectionParameters;
    private PeerConnectionFactory factory;
    private MediaConstraints pcConstraints;
    private MediaStream local_mediaStream;
    private VideoTrack mVideoTrack;
    private AudioTrack localAudioTrack;
    private String CONSTANT_AUDIO_ID = "audio_id";
    private LocalMediaStreamListener mLocalMediaStreamListener;



    public VideoView(Context context) {
        super(context);
    }

    public VideoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public VideoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

    }

    public void init(EglBase rootEglBase) {
        eglBase = rootEglBase;
        videoViewCallback.add(mSurfaceViewRenderer);
        mSurfaceViewRenderer.init(rootEglBase.getEglBaseContext(), null);
        mSurfaceViewRenderer.setZOrderMediaOverlay(false);
        mSurfaceViewRenderer.setEnableHardwareScaler(true);
        mSurfaceViewRenderer.setMirror(true);

    }



    public void setOverlayView(boolean mOverlayView) {
        mSurfaceViewRenderer.setZOrderMediaOverlay(mOverlayView);
    }

    public void setScalingType(RendererCommon.ScalingType mType) {
        type = mType;
    }

    public void updateView() {
        mSurfaceViewRenderer.setScalingType(type);
        mSurfaceViewRenderer.setMirror(false);
        mPercentFrameLayout.requestLayout();
        mSurfaceViewRenderer.setEnableHardwareScaler(true);
    }

    public  void  simple(){}

    public void setRemoteMediaStream(MediaStream mStream) {
        if (mSurfaceViewRenderer.getVisibility() == View.INVISIBLE) {
            mSurfaceViewRenderer.setVisibility(View.VISIBLE);
            mPercentFrameLayout.setVisibility(View.VISIBLE);
        }
        remoteStream = mStream;
        VideoTrack videoTrack = mStream.videoTracks.get(0);
        videoTrack.setEnabled(true);
        if (videoRenderer != null && currentVideoTrack != null)
            currentVideoTrack.removeRenderer(videoRenderer);
        for (VideoRenderer.Callbacks remoteRender : videoViewCallback) {
            videoRenderer = new VideoRenderer(remoteRender);
            currentVideoTrack = videoTrack;
            videoTrack.addRenderer(videoRenderer);
        }
    }

    public void removeMediaStream() {
        MediaStream mediaStream = null;
        mediaStream = local_mediaStream;

        if (mediaStream == null) {
            mediaStream = remoteStream;
        }

        if (mediaStream==null)
        {
            return;
        }
        mPercentFrameLayout.setVisibility(View.INVISIBLE);

        for (VideoTrack videoTrack : mediaStream.videoTracks) {
            mediaStream.removeTrack(videoTrack);

        }

        for (AudioTrack audioTrack : mediaStream.audioTracks) {
            mediaStream.removeTrack(audioTrack);
        }


        mediaStream = null;
    }

    public void createLocalStream(PeerConnectionParameters peerConnectionParameters, LocalMediaStreamListener localMediaStreamListener) {
        mLocalMediaStreamListener = localMediaStreamListener;
        PeerConnectionFactory.initializeAndroidGlobals(getContext(), true, true, true);
        this.peerConnectionParameters = peerConnectionParameters;
        factory = new PeerConnectionFactory();
        startCamera();
    }

    private void startCamera() {
        Log.v(TAG, "startCamera: ");
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                pcConstraints = new MediaConstraints();
                pcConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"));
                pcConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"));
                factory.setVideoHwAccelerationOptions(eglBase.getEglBaseContext(), eglBase.getEglBaseContext());
                local_mediaStream = factory.createLocalMediaStream(getMediaStreamId());
                mVideoTrack = createVideoTrack(getVideoCapturer(new Camera2Enumerator(getContext())));

                local_mediaStream.addTrack(mVideoTrack);
                AudioSource audioSource = factory.createAudioSource(new MediaConstraints());
                localAudioTrack = factory.createAudioTrack(CONSTANT_AUDIO_ID, audioSource);
                local_mediaStream.addTrack(localAudioTrack);
                Log.d(TAG, "localStream" + local_mediaStream);
                mLocalMediaStreamListener.onLocalStream(local_mediaStream, factory, getMediaConstraint());
            }
        });
        t.start();
    }

    public VideoTrack createVideoTrack(VideoCapturer capturer) {

        VideoSource videoSource = factory.createVideoSource(capturer);
        capturer.startCapture(peerConnectionParameters.videoWidth, peerConnectionParameters.videoHeight, peerConnectionParameters.videoFps);
        VideoTrack localVideoTrack = factory.createVideoTrack(CONSTANT_VIDEO_ID, videoSource);

        localVideoTrack.setEnabled(true);
        if (videoRenderer != null) {
            localVideoTrack.removeRenderer(videoRenderer);
        }
        videoRenderer = new VideoRenderer(mSurfaceViewRenderer);


        localVideoTrack.addRenderer(videoRenderer);

        currentVideoTrack = localVideoTrack;
        return localVideoTrack;
    }


    private VideoCapturer getVideoCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();
        Log.d(TAG, "Looking for front facing cameras.");

        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Log.d(TAG, "Creating front facing camera capturer." + deviceName);
                videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Log.d(TAG, "Creating other camera capturer.");
                videoCapturer = enumerator.createCapturer(deviceName, null);
                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }
        return videoCapturer;
    }

    public MediaStream getRemoteStream() {
        if (remoteStream == null) {
            return local_mediaStream;
        } else {
            return remoteStream;
        }
    }

    public PeerConnectionFactory getFactory() {
        return factory;
    }

    public MediaConstraints getMediaConstraint() {
        return pcConstraints;
    }

    public MediaStream getLocalStream() {
        return local_mediaStream;
    }

    public void onToggleMic(boolean mic) {
        if (localAudioTrack != null) {
            localAudioTrack.setEnabled(mic);
        }
    }

    public void setVideoEnable(boolean flag) {
        mVideoTrack.setEnabled(flag);
    }

    public void enableSpeaker(boolean isOn, Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (isOn) {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            audioManager.setMode(AudioManager.MODE_NORMAL);
        } else {
            audioManager.setMode(AudioManager.MODE_NORMAL);
            audioManager.setMode(AudioManager.MODE_IN_CALL);
        }
        audioManager.setSpeakerphoneOn(isOn);
    }

    public void switchCameraInternal() {
        if (videoCapturer instanceof CameraVideoCapturer) {
            CameraVideoCapturer cameraVideoCapturer = (CameraVideoCapturer) videoCapturer;
            cameraVideoCapturer.switchCamera(new CameraVideoCapturer.CameraSwitchHandler() {
                @Override
                public void onCameraSwitchDone(boolean b) {

                }

                @Override
                public void onCameraSwitchError(String s) {

                }
            });
        } else {

            Log.d(TAG, "Will not switch camera, video caputurer is not a camera");

        }

    }


    public String getMediaStreamId() {
        return "localMediaStreamId";
    }
}
