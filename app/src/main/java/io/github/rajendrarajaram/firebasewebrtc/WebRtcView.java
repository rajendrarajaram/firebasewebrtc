package io.github.rajendrarajaram.firebasewebrtc;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.hsalf.smilerating.SmileRating;

import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoFrame;

public class WebRtcView extends FrameLayout {
    private  String TAG  = WebRtcView.class.getSimpleName();
    protected PercentFrameLayout mPercentFrameLayout;
    protected CustomViewRenderer mSurfaceViewRenderer;
    protected SmileRating smileRating;

    public WebRtcView(Context context) {
        super( context );
        inflateView();
        customView();
        addView();


    }

    private void addView() {
        LayoutParams params = new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
        addView( mPercentFrameLayout, params );
    }

    private void customView() {
        mSurfaceViewRenderer = mPercentFrameLayout.findViewById( R.id.video );
        smileRating = mPercentFrameLayout.findViewById(R.id.smile_rating);

    }

    public void rendererCallback(CustomViewRenderer.BitmapCallback bitmapCallback)
    {
        mSurfaceViewRenderer.setBitmapCallback(bitmapCallback);

    }

    private void inflateView() {
        mPercentFrameLayout = (PercentFrameLayout) LayoutInflater.from( getContext() ).inflate( R.layout.video_view, null, false );

    }

    public WebRtcView(Context context, @Nullable AttributeSet attrs) {
        super( context, attrs );
        inflateView();
        customView();
        addView();
    }


    public WebRtcView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super( context, attrs, defStyleAttr );
        inflateView();
        customView();
        addView();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WebRtcView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super( context, attrs, defStyleAttr, defStyleRes );
        inflateView();
        customView();
        addView();
    }






    public void simple(float index)
    {
        if (index<=.2)
        {
            smileRating.setSelectedSmile(SmileRating.TERRIBLE,true);

        }else if(index<=.4)
        {
            smileRating.setSelectedSmile(SmileRating.BAD,true);

        }else if (index<=.6)
        {
            smileRating.setSelectedSmile(SmileRating.OKAY,true);

        }else  if(index<=.8)
        {
            smileRating.setSelectedSmile(SmileRating.GREAT,true);

        }

    }
}
