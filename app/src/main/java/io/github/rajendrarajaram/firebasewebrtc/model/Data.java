package io.github.rajendrarajaram.firebasewebrtc.model;

public class Data {
    private  String id;
    private SubData subData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SubData getSubData() {
        return subData;
    }

    public void setSubData(SubData subData) {
        this.subData = subData;
    }
}
