package io.github.rajendrarajaram.firebasewebrtc.model;

public class SmileData {
    public  String id;

    public  float smileIndex;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getSmileIndex() {
        return smileIndex;
    }

    public void setSmileIndex(float smileIndex) {
        this.smileIndex = smileIndex;
    }
}
