package io.github.rajendrarajaram.firebasewebrtc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubData {


    @Expose
    @SerializedName("ice")
    private Ice ice;
    @Expose
    @SerializedName("sdp")
    private Sdp sdp;

    @Expose
    @SerializedName("invite")
    private Invite invite;

    public Invite getInvite() {
        return invite;
    }

    public void setInvite(Invite invite) {
        this.invite = invite;
    }

    public Ice getIce() {
        return ice;
    }

    public void setIce(Ice ice) {
        this.ice = ice;
    }

    public Sdp getSdp() {
        return sdp;
    }

    public void setSdp(Sdp sdp) {
        this.sdp = sdp;
    }

    public static class Ice {
        @Expose
        @SerializedName("sdpMLineIndex")
        private int sdpMLineIndex;
        @Expose
        @SerializedName("sdpMid")
        private String sdpMid;
        @Expose
        @SerializedName("candidate")
        private String candidate;

        public int getSdpMLineIndex() {
            return sdpMLineIndex;
        }

        public void setSdpMLineIndex(int sdpMLineIndex) {
            this.sdpMLineIndex = sdpMLineIndex;
        }

        public String getSdpMid() {
            return sdpMid;
        }

        public void setSdpMid(String sdpMid) {
            this.sdpMid = sdpMid;
        }

        public String getCandidate() {
            return candidate;
        }

        public void setCandidate(String candidate) {
            this.candidate = candidate;
        }
    }


    public static class Sdp {
        @Expose
        @SerializedName("sdp")
        private String sdp;
        @Expose
        @SerializedName("type")
        private String type;

        public String getSdp() {
            return sdp;
        }

        public void setSdp(String sdp) {
            this.sdp = sdp;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }



    public static class  Invite
    {
      private   String dd;

        public String getDd() {
            return dd;
        }

        public void setDd(String dd) {
            this.dd = dd;
        }
    }


}
