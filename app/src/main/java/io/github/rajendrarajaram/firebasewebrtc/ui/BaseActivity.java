package io.github.rajendrarajaram.firebasewebrtc.ui;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.Duration;

public class BaseActivity extends AppCompatActivity {

    private String[] permission;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {

        super.onCreate(savedInstanceState, persistentState);

    }


    public void checkRequiredPermission(String[] permission) {
        this.permission = permission;
        checkPermissions(permission);
    }


    /**
     * called when required permission is granted to notify in child class need to override this
     */
    protected void invokedWhenPermissionGranted() {

    }

    /**
     * called when required permission is not or already granted to notify in child class need to override this
     */
    protected void invokedWhenNoOrAlreadyPermissionGranted() {

    }

    /**
     * check the permission
     *
     * @param permission
     */
    private void checkPermissions(String... permission) {

        if (Build.VERSION.SDK_INT >= 23 && permission != null) {

            int result;
            List<String> listPermissionsNeeded = new ArrayList<>();
            for (String p : permission) {
                result = ContextCompat.checkSelfPermission(this, p);
                if (result != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(p);
                }
            }
            if (!listPermissionsNeeded.isEmpty()) {

                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 111);

            } else {

                invokedWhenNoOrAlreadyPermissionGranted();

            }

        } else {

            invokedWhenNoOrAlreadyPermissionGranted();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 111 && hasAllPermissionsGranted(grantResults)) {
            allPermissionsGranted();
        } else if (requestCode == 111) {
            invokedWhenDeniedWithResult(grantResults);
        }
    }

    /**
     * called when all required permission is checked and granted
     */
    private void allPermissionsGranted() {
        invokedWhenPermissionGranted();
    }

    /**
     * check and show denied permission to notify in child class need to Override this
     *
     * @param grantResults
     */
    protected void invokedWhenDeniedWithResult(int[] grantResults) {

    }

    /**
     * check all permission granted
     *
     * @param grantResults
     * @return
     */
    private boolean hasAllPermissionsGranted(@NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    protected void showSnakeBar(View view,String msg)
    {
        Snackbar.make(view,msg, Snackbar.LENGTH_LONG).show();
    }



}
