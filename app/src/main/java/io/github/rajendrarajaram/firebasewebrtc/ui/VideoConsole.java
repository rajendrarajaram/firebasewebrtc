package io.github.rajendrarajaram.firebasewebrtc.ui;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import io.github.rajendrarajaram.firebasewebrtc.MainActivity;
import io.github.rajendrarajaram.firebasewebrtc.R;

public class VideoConsole implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private MainActivity mainActivity;
    private CheckBox mic;
    private ImageView switchCam,message;


    public VideoConsole(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        mic = this.mainActivity.findViewById(R.id.mic);
        switchCam = this.mainActivity.findViewById(R.id.switch_camera);
        message = this.mainActivity.findViewById(R.id.message);
        mic.setOnCheckedChangeListener(this);
        switchCam.setOnClickListener(this);
        message.setOnClickListener(this);


    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        mainActivity.mVideoView_local.onToggleMic(b);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.switch_camera:
                mainActivity.mVideoView_local.switchCameraInternal();
                break;
            case R.id.message:
                break;
        }
    }
}
